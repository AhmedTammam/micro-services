import ListingService from '#root/adapters/listingService'


const listingResolver = async () => {
    return await ListingService.fetchAllListings();
};

export default listingResolver;