import ListingService from '#root/adapters/listingService';

const createListingResolver = async (obj, { description, title }, context) => {
    if(!context.res.locals.userSession) return new Error("Not Logged in");
    
    return await ListingService.createListing({description, title});
}

export default createListingResolver;