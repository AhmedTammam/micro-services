//actions

const CLEAR = "session/CLEAR";
const SET = "session/SET";

//actionCreators

export const setSession = session => {
    return {
        type: SET,
        session
    }
}

export const clearSession = () => {
    return {
        type: CLEAR
    }
}



//reducer

const initialState = null;

const sessionReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET:
            return action.session;
        case CLEAR:
            return null;
    }

    return state;
}

export default sessionReducer;
