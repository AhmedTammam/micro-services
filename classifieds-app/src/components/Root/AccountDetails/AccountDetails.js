import React, { useState } from 'react';
import { useSelector } from 'react-redux'

import Account from './Account/Account'
import Login from './Login'
import Signup from './Signup'

const AccountDetails = () => {
    const [isSigningUp, setIsSigningup] = useState(false)
    const session = useSelector(state => state.session);

    if(session) return <Account />;

    return isSigningUp ? <Signup onChangeToLogin ={()=> setIsSigningup(false)}/> : <Login onChangeToSignup={() => setIsSigningup(true)} />
}


export default AccountDetails;