import React from 'react';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag'
import { useSelector, useDispatch } from 'react-redux'

import styled from 'styled-components';

import {clearSession } from '#root/store/ducks/session'

const Wrapper = styled.div`
    color : ${props => props.theme.mortar};
    font-size: 15px;
`;

const Email = styled.div`
    color: ${props => props.theme.nero};
    font-weight: bold;
`;

const Logout = styled.a.attrs({href: "#"})`
    color: blue;
    display: block;
    margin-top: 10px;
`;

const mutation = gql`
    mutation ($sessionId: ID!) {
        deleteUserSession(sessionId: $sessionId)
    }
`;

const Account = () => {
    const session = useSelector(state => state.session);

    const [deleteUserSession ] = useMutation(mutation);

    const dispatch = useDispatch();

    return (
        <Wrapper>
            Welcome <Email>{session.user.email}</Email>
            <Logout onClick={e => {
                e.preventDefault();
                dispatch(clearSession());
                deleteUserSession({ variables: { sessionId: session.id}})
            }}>Logout</Logout>
        </Wrapper>
    )
}

export default Account;