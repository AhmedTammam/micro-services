import { Sequelize } from 'sequelize';

import accessEnv from '#root/helpers/accessEnv';

const DB_URI = accessEnv("DB_URI");
// DB_HOST
// DB_NAME
// DB_password
// DB_username

const sequelize = new Sequelize(DB_URI, {
    dialectOptions: {
        charset: "utf8",
        multipleStatements: true
    },
    loggign: false
});


export default sequelize;